# Unity_GameObjectFilter

A utility class used to filter gameobjects in scenerio like Physics casting.

## Usage
Set up the filter in inspector or by code, then call **Match(GameObject g)**.
Target filter has priority over Ignore ones, basically *Target* means you just want these.